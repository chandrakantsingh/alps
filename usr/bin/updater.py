import os
import sys
sys.path.append('.')

def getVersion():
	with open('/etc/lsb-release') as lsb_release:
		for line in lsb_release:
			if line.startswith('DISTRIB_RELEASE'):
				version = line.replace('DISTRIB_RELEASE=', '').replace('"', '').strip()
				return version
	return None

def getInstalledDesktopEnvironments():
	installed_desktop_environments = list()
	with open('/etc/alps/installed-list') as installed_list:
		for line in installed_list:
			if line.startswith('xfce-desktop-environment') or line.startswith('mate-desktop-environment') or line.startswith('gnome-desktop-environment') or line.startswith('kde-desktop-environment'):
				installed_desktop_environments.append(line.split('=')[0])
	return installed_desktop_environments

def getLatestversion():
	pass

def createDownloadUrl(baseUrl, version, component):
	pass

def downloadUpdateTarball(url, tmpDir):
	pass

def decompressAndMerge(tarballPath):
	pass

def getInstalledPackages():
	# Get the list of packages that were installed
	# after the desktop environment.
	# In case of big packages add the bin package to the list
	pass

def createUpdaterScript():
	# Create a script that would alps reinstall the above
	# list.
	pass

def runUpdate():
	pass
