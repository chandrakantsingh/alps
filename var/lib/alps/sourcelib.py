import os
import sys
import subprocess
sys.path.append(".")

def downloadSource(url, insecure):
	cmd = "wget -nc "
	if insecure == True:
		cmd = cmd + " --no-check-certificate "
	cmd = cmd + url
	subprocess.Popen(url.split(), shell=True).communicate()

def clearCache(config):
	cmd = "sudo rm -rf " + config["SOURCE_DIR"]
	subprocess.Popen(cmd.split(), shell=True).communicate()
