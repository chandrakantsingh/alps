#!/bin/bash

set -e
set +h

. /etc/alps/alps.conf

repo_url="$1"
directory=$(echo $repo_url | rev | cut -d/ -f1 | rev)
if echo $repo_url | grep "\.git$" &> /dev/null; then
	directory=$(echo $directory | sed "s@\.git\$@@g")
fi

pushd $SOURCE_DIR

sudo rm -rf $directory
git clone $repo_url
pushd $directory

if [ -f "./autogen.sh" ]; then
	CONF_SYSTEM="autogen"
elif [ -f "./configure" ]; then
	CONF_SYSTEM="configure"
elif [ -f "./CMakeLists.txt" ]; then
	CONF_SYSTEM="cmake"
elif [ -f "Makefile" ]; then
	CONF_SYSTEM="makefile"
elif [ -f "meson.build" ]; then
	CONF_SYSTEM="meson"
else
	echo "Could not figure out a way to build and install from this source automatically. Exiting..."
	exit
fi

FAILED="yes"

if [ "$CONF_SYSTEM" == "autogen" ]; then
	./autogen.sh --prefix=/usr &&
	./configure --prefix=/usr &&
	make -j$(nproc) &&
	sudo make install
	FAILED="no"
elif [ "$CONF_SYSTEM" == "configure" ]; then
	./configure --prefix=/usr &&
	make -j$(nproc) &&
	sudo make install
	FAILED="no"
elif [ "$CONF_SYSTEM" == "cmake" ]; then
	mkdir build
	cd build
	cmake -DCMAKE_INSTALL_PREFIX=/usr .. &&
	make -j$(nproc) &&
	sudo make install
	FAILED="no"
elif [ "$CONF_SYSTEM" == "meson" ]; then
	mkdir build
	cd build
	meson --prefix=/usr &&
	ninja &&
	sudo ninja install
	FAILED="no"
elif [ "$CONF_SYSTEM" == "makefile" ]; then
	sed -i "s@/usr/local@/usr@g" Makefile
	make -j$(nproc) &&
	sudo make install
	FAILED="no"
fi

popd

if [ "$FAILED" == "no" ]; then
	echo "Source downloaded and installed successfully."
	rm -rf $directory
else
	echo "Source could not be installed successfully."
fi