#!/usr/bin/python3

def initConfig(confFile):
	config = dict()
	lines = open(confFile).readlines()
	for line in lines:
        	parts = line.split("=")
	        config[parts[0]] = parts[1]
	return config

