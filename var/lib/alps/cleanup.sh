#!/bin/bash

DIRS="/var/cache/alps/sources
/var/cache/alps/binaries"

echo "Cleaning up alps work directories..."
for DIR in $(echo $DIRS); do

pushd /var/cache/alps/sources/ &> /dev/null
sudo rm -rf *
popd &> /dev/null

done

echo "alps cleanup completed."